#ifndef LOG_H
#define LOG_H

#include <iostream>
#include <sstream>
#include <chrono>
#include <ctime>
#include <iomanip>

#define LENABLE             Log::enable();
#define LDISABLE            Log::disable();
#define LFILTER(_filter_)   Log::setFilter(_filter_);
#define LDEBUG(...)         Log::debug(__VA_ARGS__);
#define LSDEBUG(...)        Log::debug_s(__VA_ARGS__);
#define LINFO(...)          Log::info(__VA_ARGS__);
#define LSINFO(...)         Log::info_s(__VA_ARGS__);
#define LWARN(...)          Log::warning(__VA_ARGS__);
#define LSWARN(...)         Log::warning_s(__VA_ARGS__);
#define LERR(...)           Log::error(__VA_ARGS__);
#define LSERR(...)          Log::error_s(__VA_ARGS__);
#define LFATAL(...)         Log::fatal(__VA_ARGS__);
#define LSFATAL(...)        Log::fatal_s(__VA_ARGS__);

class Log
{
public:
    enum Level {
        L_DEBUG = 0x0001,
        L_INFO = 0x0002,
        L_WARNING = 0x0004,
        L_ERROR = 0x0008,
        L_FATAL = 0x0010
    };

    template <typename... Args>
    static void debug(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & L_DEBUG))
            return;
        log.push("DEBUG", false, std::forward<Args>(args)...);
    }

    template <typename... Args>
    static void debug_s(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & L_DEBUG))
            return;
        log.push("DEBUG", true, std::forward<Args>(args)...);
    }

    template <typename... Args>
    static void info(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & L_INFO))
            return;
        log.push("INFO", false, std::forward<Args>(args)...);
    }

    template <typename... Args>
    static void info_s(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & L_INFO))
            return;
        log.push("INFO", true, std::forward<Args>(args)...);
    }

    template <typename... Args>
    static void warning(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & L_WARNING))
            return;
        log.push("WARNING", false, std::forward<Args>(args)...);
    }

    template <typename... Args>
    static void warning_s(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & L_WARNING))
            return;
        log.push("WARNING", true, std::forward<Args>(args)...);
    }

    template <typename... Args>
    static void error(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & L_ERROR))
            return;
        log.push("ERROR", false, std::forward<Args>(args)...);
    }

    template <typename... Args>
    static void error_s(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & L_ERROR))
            return;
        log.push("ERROR", true, std::forward<Args>(args)...);
    }

    template <typename... Args>
    static void fatal(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & L_FATAL))
            return;
        log.push("FATAL", false, std::forward<Args>(args)...);
    }

    template <typename... Args>
    static void fatal_s(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & L_FATAL))
            return;
        log.push("FATAL", true, std::forward<Args>(args)...);
    }

    static void enable() {
        instance().m_enable = true;
    }
    static void disable() {
        instance().m_enable = false;
    }
    static void setFilter(int filter) {
        instance().m_filter = filter;
    }

private:
    static Log &instance();

    Log() : m_enable(false), m_filter(L_DEBUG | L_INFO | L_WARNING | L_ERROR | L_FATAL) {}

    template <typename... Args>
    void push(const std::string &level, bool spacing, Args &&...args) {
        std::string s = spacing ? " " : "";
        m_stream << '[' << currentTime() << ']';
        m_stream << '[' << level << ']' << " ";
        using expander = int[];
        (void)expander{0, (void(m_stream << std::forward<Args>(args) << s), 0)...};
        print();
    }

    template <typename Arg>
    void push(std::stringstream &stream, Arg &&arg) {
        stream << std::forward<Arg>(arg);
    }

    std::string currentTime() const {
        std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
        std::time_t now_c = std::chrono::system_clock::to_time_t(now - std::chrono::hours(24));
        std::string currentTime = std::ctime(&now_c);
        currentTime.pop_back();
        return currentTime;
    }

    void print() {
        std::cout << m_stream.str() << std::endl;
        m_stream.str(std::string());
        m_stream.clear();
    }

private:
    std::stringstream m_stream;
    bool m_enable;
    int m_filter;

};

#endif // LOG_H
