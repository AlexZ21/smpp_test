cmake_minimum_required (VERSION 2.8)

project (network_log)

set(CMAKE_POSITION_INDEPENDENT_CODE ON)

file(GLOB_RECURSE NETWORK_LOG_SRC
    src/*.cpp
    src/*.c
    )

add_library(${PROJECT_NAME} STATIC ${NETWORK_LOG_SRC})
