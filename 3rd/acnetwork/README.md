# README #

C++ library for network programming.

### Usage ###
```C++
#include <acnetwork/networkclient.h>
#include <iostream>

int main(int argc, char *argv[])
{
    ac::NetworkClient client;
    client.onReady([](const ac::Response &response){
        std::cout << response.body() << std::endl;
    });
    client.sendRequest(ac::Request(ac::Url("https://bitbucket.org")));
    client.waitForFinish();

    return 0;
}
```