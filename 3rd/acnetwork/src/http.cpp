#include <acnetwork/http.h>
#include <acnetwork/ipaddress.h>

#include <iostream>

namespace ac {

Http::Http() :
    m_running(false)
{
    storeBody = true;
}

Response Http::sendRequest(const Request &request, const std::chrono::milliseconds &maxTimeout)
{
    Request toSend(request);

    IpAddress host(toSend.url().host());
    uint16_t port = toSend.url().port().empty() ? 80 : std::stoi(toSend.url().port());

    if (!toSend.hasField("From"))
        toSend.setField("From", "user@acnetwork.com");

    if (!toSend.hasField("User-Agent"))
        toSend.setField("User-Agent", "acnetwork/1.x");

    if (!toSend.hasField("Host"))
        toSend.setField("Host", toSend.url().host());

    if (!toSend.hasField("Content-Length"))
        toSend.setField("Content-Length", std::to_string(toSend.body().size()));

    if ((toSend.method() == Request::Post) && !toSend.hasField("Content-Type"))
        toSend.setField("Content-Type", "application/x-www-form-urlencoded");

    if ((toSend.httpMajorVersion() * 10 + toSend.httpMinorVersion() >= 11) && !toSend.hasField("Connection"))
        toSend.setField("Connection", "close");

    Response received;

    if (m_socket.connect(host, port, std::chrono::seconds(5)) == Socket::Done) {
        std::string requestStr = toSend.toString();

        if (!requestStr.empty()) {
            if (m_socket.send(requestStr.c_str(), requestStr.size()) == Socket::Done) {
                std::string receivedStr;
                std::size_t size = 0;
                char buffer[1024];

                bool headerReady = false;
                size_t lastHeaderFind = 0;

                int64_t msec = maxTimeout.count();
                m_running.store(true);
                while (m_running.load()) {
                    // Timeout 500ms setting to stop receiving the data
                    Socket::Status status = m_socket.receive(buffer, sizeof(buffer), size, std::chrono::milliseconds(500));

                    if (status == Socket::Timeout) {
                        msec -= 500;
                        if (msec <= 0)
                            m_running.store(false);
                        continue;
                    }

                    if (status != Socket::Done && status != Socket::Timeout) {
                        m_running.store(false);
                        continue;
                    }

                    msec = maxTimeout.count();
                    if (size > 0)
                        receivedStr.append(buffer, size);

                    // Header ready check
                    if (!headerReady) {
                        size_t pos = receivedStr.find("\r\n\r\n", lastHeaderFind);
                        if (pos != std::string::npos) {
                            headerReady = true;
                            Response::Header header;
                            header.fromString(receivedStr);
                            received.setHeader(header);
                            receivedStr.erase(0, pos + 4);

                            if (onHeaderReadyCallback)
                                onHeaderReadyCallback(header);

                            if (onDataCallback && receivedStr.size() > 0)
                                onDataCallback(receivedStr.c_str(), receivedStr.size());

                        } else {
                            lastHeaderFind = receivedStr.size() - 4;
                        }

                    } else {
                        if (onDataCallback)
                            onDataCallback(buffer, size);
                    }
                }

                m_running.store(false);

                if (storeBody)
                    received.setBody(receivedStr);
            }
        }

        m_socket.disconnect();
    }

    if (onReadyCallback)
        onReadyCallback(received);

    return received;
}

void Http::stop()
{
    m_running.store(false);
}

}
