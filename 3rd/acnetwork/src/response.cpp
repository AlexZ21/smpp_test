#include <acnetwork/response.h>

#include <algorithm>
#include <sstream>

namespace {
std::string toLower(std::string str) {
    if (!str.empty())
        std::transform(str.begin(), str.end(), str.begin(), ::tolower);
    return str;
}
}

namespace ac {

Response::Header::Header() :
    m_status(ConnectionFailed),
    m_majorVersion(0),
    m_minorVersion(0)
{

}


Response::Status Response::Header::status() const
{
    return m_status;
}

std::string Response::Header::field(const std::string &field) const
{
    auto it = m_fields.find(toLower(field));
    if (it == m_fields.end())
        return std::string();
    return it->second;
}

uint32_t Response::Header::majorHttpVersion() const
{
    return m_majorVersion;
}

uint32_t Response::Header::minorHttpVersion() const
{
    return m_minorVersion;
}


void Response::Header::fromString(const std::string &str)
{
    std::istringstream in(str);

    std::string version;
    if (in >> version) {
        if ((version.size() >= 8) && (version[6] == '.') &&
            (toLower(version.substr(0, 5)) == "http/")   &&
             isdigit(version[5]) && isdigit(version[7]))
        {
            m_majorVersion = version[5] - '0';
            m_minorVersion = version[7] - '0';
        } else {
            m_status = InvalidResponse;
            return;
        }
    }

    int status;
    if (in >> status) {
        m_status = static_cast<Status>(status);
    } else {
        m_status = InvalidResponse;
        return;
    }

    in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

    m_fields.clear();
    parseFields(in);
}

void Response::Header::parseFields(std::istream &in)
{
    std::string line;
    while (std::getline(in, line) && (line.size() > 2)) {
        std::string::size_type pos = line.find(": ");
        if (pos != std::string::npos) {
            std::string field = line.substr(0, pos);
            std::string value = line.substr(pos + 2);

            if (!value.empty() && (*value.rbegin() == '\r'))
                value.erase(value.size() - 1);

            m_fields[toLower(field)] = value;
        }
    }
}

const Response::Header &Response::header() const
{
    return m_header;
}

void Response::setHeader(const Response::Header &header)
{
    m_header = header;
}

const std::string &Response::body() const
{
    return m_body;
}

void Response::setBody(const std::string &body)
{
    m_body = body;
}

}
