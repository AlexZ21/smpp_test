#include <acnetwork/request.h>

#include <sstream>
#include <algorithm>

namespace {
std::string toLower(std::string str) {
    std::transform(str.begin(), str.end(), str.begin(), ::tolower);
    return str;
}
}

namespace ac {

Request::Request(const Url &url, Method method, const std::string& body) :
    m_url(url),
    m_method(method),
    m_body(body),
    m_majorVersion(1),
    m_minorVersion(0)
{

}

Request::Request(const Request &other) :
    m_url(other.m_url),
    m_method(other.m_method),
    m_body(other.m_body),
    m_fields(other.m_fields),
    m_majorVersion(other.m_majorVersion),
    m_minorVersion(other.m_minorVersion)
{

}

Request &Request::operator =(const Request &other)
{
    m_url = other.m_url;
    m_method = other.m_method;
    m_body = other.m_body;
    m_fields = other.m_fields;
    m_majorVersion = other.m_majorVersion;
    m_minorVersion = other.m_minorVersion;
    return *this;
}

Url Request::url() const
{
    return m_url;
}

void Request::setUrl(const Url &url)
{
    m_url = url;
}

Request::Method Request::method() const
{
    return m_method;
}

void Request::setMethod(Request::Method method)
{
    m_method = method;
}

std::string Request::body() const
{
    return m_body;
}

void Request::setBody(const std::string &body)
{
    m_body = body;
}

const std::map<std::string, std::string> &Request::fields() const
{
    return m_fields;
}

void Request::setField(const std::string &field, const std::string &value)
{
    m_fields.emplace(toLower(field), value);
}

bool Request::hasField(const std::string &field) const
{
    return m_fields.find(toLower(field)) != m_fields.end();
}

uint32_t Request::httpMajorVersion() const
{
    return m_majorVersion;
}

uint32_t Request::httpMinorVersion() const
{
    return m_minorVersion;
}

void Request::setHttpVersion(uint32_t major, uint32_t minor)
{
    m_majorVersion = major;
    m_minorVersion = minor;
}

std::string Request::toString() const
{
    std::ostringstream out;

    std::string method;
    switch (m_method) {
    case Get: method = "GET"; break;
    case Post: method = "POST"; break;
    case Head: method = "HEAD"; break;
    case Put: method = "PUT"; break;
    case Delete: method = "DELETE"; break;
    }

    std::string uri = m_url.uri();
    if (uri.empty())
        uri = "/";
    out << method << " " << uri << " ";
    out << "HTTP/" << m_majorVersion << "." << m_minorVersion << "\r\n";

    for (auto &it : m_fields)
        out << it.first << ": " << it.second << "\r\n";

    out << "\r\n";
    out << m_body;

    return out.str();
}

}
