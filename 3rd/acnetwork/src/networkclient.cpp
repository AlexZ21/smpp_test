#include <acnetwork/networkclient.h>
#include <acnetwork/http.h>
#include <acnetwork/https.h>

#include <iostream>

namespace ac {

NetworkClient::NetworkClient() :
    m_followLocation(true),
    m_storeBody(true)
{

}

NetworkClient::~NetworkClient()
{
    stop();
}

void NetworkClient::sendRequest(const Request &request, const std::chrono::milliseconds &maxTimeout)
{
    if (m_thread.joinable()) {
        std::cout << "Request is being processed" << std::endl;
        return;
    }
    m_thread = std::thread(&NetworkClient::threadFunc, this, request, maxTimeout);
}

void NetworkClient::waitForFinish()
{
    if (m_thread.joinable())
        m_thread.join();
}

void NetworkClient::stop()
{
    if (m_http.get())
        m_http->stop();
    if (m_thread.joinable())
        m_thread.join();
}

void NetworkClient::setFollowLocation(bool followLocation)
{
    m_followLocation = followLocation;
}

void NetworkClient::setStoreBody(bool storeBody)
{
    m_storeBody = storeBody;
}

void NetworkClient::onHeaderReady(const std::function<void (const Response::Header &)> &onHeaderReadyCallback)
{
    m_onHeaderReadyCallback = onHeaderReadyCallback;
}

void NetworkClient::onData(const std::function<void (const char *, size_t)> &onDataCallback)
{
    m_onDataCallback = onDataCallback;
}

void NetworkClient::onReady(const std::function<void (const Response &)> &onReadyCallback)
{
    m_onReadyCallback = onReadyCallback;
}

void NetworkClient::threadFunc(Request request, std::chrono::milliseconds maxTimeout)
{
    Url url = request.url();
    while (true) {
        m_http.reset(url.protocol() == "http" ? (HttpAbstract*)new Http() : (HttpAbstract*)new Https());
        m_http->setStoreBody(m_storeBody);

        bool redirect = false;
        m_http->onHeaderReady([this, &url, &redirect](const Response::Header &header){
            if (header.status() == 302 || header.status() == 301 || header.status() == 300) {
                url.setUrl(header.field("location"));
                redirect = true;
                m_http->stop();
            }
        });

        if (m_onDataCallback) {
            m_http->onData([this, &redirect](const char *data, size_t size){
                if (!redirect)
                    m_onDataCallback(data, size);
            });

        }

        Request toSend(request);
        toSend.setUrl(url);
        Response response = m_http->sendRequest(toSend, maxTimeout);

        if (redirect)
            continue;

        if (m_onReadyCallback)
            m_onReadyCallback(response);

        return;
    }
}

}
