#ifndef HTTP_H
#define HTTP_H

#include "acnetwork_export.h"
#include "httpabstract.h"
#include "tcpsocket.h"

#include <atomic>

namespace ac {

class ACNETWORK_EXPORT Http : public HttpAbstract
{
public:
    Http();
    ~Http() = default;

    Response sendRequest(const Request &request, const std::chrono::milliseconds &maxTimeout = std::chrono::milliseconds(10000));
    void stop();

private:
    TcpSocket m_socket;
    std::atomic_bool m_running;

};

}

#endif // HTTP_H
