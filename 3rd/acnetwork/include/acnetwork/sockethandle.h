#ifndef SOCKETHANDLE_H
#define SOCKETHANDLE_H

#include "acnetwork_export.h"

#if defined(SYSTEM_WIN)
    #include <basetsd.h>
#endif

namespace ac {

#if defined(SYSTEM_WIN)
    typedef UINT_PTR SocketHandle;
#else
    typedef int SocketHandle;
#endif

}

#endif // SOCKETHANDLE_H
