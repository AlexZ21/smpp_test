#ifndef HTTPABSTRACT_H
#define HTTPABSTRACT_H

#include "acnetwork_export.h"
#include "request.h"
#include "response.h"

#include <chrono>
#include <functional>

namespace ac {

class HttpAbstract
{
public:
    HttpAbstract() = default;
    virtual ~HttpAbstract() = default;

    virtual Response sendRequest(const Request &request,
                                 const std::chrono::milliseconds &maxTimeout = std::chrono::milliseconds(10000)) = 0;
    virtual void stop() = 0;

    void setStoreBody(bool storeBody_) { storeBody = storeBody_; }

    void onHeaderReady(const std::function<void (const Response::Header &)> &onHeaderReadyCallback_) {
        onHeaderReadyCallback = onHeaderReadyCallback_;
    }
    void onData(const std::function<void (const char *, size_t)> &onDataCallback_) {
        onDataCallback = onDataCallback_;
    }
    void onReady(const std::function<void (const Response &)> &onReadyCallback_) {
        onReadyCallback = onReadyCallback_;
    }

protected:
    bool storeBody;
    std::function<void (const Response::Header &)> onHeaderReadyCallback;
    std::function<void (const char *, size_t)> onDataCallback;
    std::function<void (const Response &)> onReadyCallback;

};

}

#endif // HTTPABSTRACT_H
