#ifndef URL_H
#define URL_H

#include "acnetwork_export.h"

#include <string>

namespace ac {

class ACNETWORK_EXPORT Url
{
public:
    Url(const std::string &url = std::string());
    ~Url() = default;

    void setUrl(const std::string &url);

    std::string protocol() const;
    std::string host() const;
    std::string port() const;
    std::string path() const;
    std::string query() const;
    std::string url() const;
    std::string uri() const;

    bool isValid() const;

private:
    std::string m_protocol;
    std::string m_host;
    std::string m_port;
    std::string m_path;
    std::string m_query;

};

}

#endif // URL_H
