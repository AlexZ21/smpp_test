#ifndef REQUEST_H
#define REQUEST_H

#include "acnetwork_export.h"
#include "url.h"

#include <map>

namespace ac {

class ACNETWORK_EXPORT Request
{
public:
    enum Method {
        Get,
        Post,
        Head,
        Put,
        Delete
    };

    Request(const Url &url, Method method = Get, const std::string& body = "");
    Request(const Request &other);
    ~Request() = default;

    Request &operator =(const Request &other);

    Url url() const;
    void setUrl(const Url &url);

    Method method() const;
    void setMethod(Method method);

    std::string body() const;
    void setBody(const std::string& body);

    const std::map<std::string, std::string> &fields() const;
    void setField(const std::string& field, const std::string& value);
    bool hasField(const std::string& field) const;

    uint32_t httpMajorVersion() const;
    uint32_t httpMinorVersion() const;
    void setHttpVersion(uint32_t major, uint32_t minor);

    std::string toString() const;

private:
    Url m_url;
    Method m_method;
    std::string m_body;
    std::map<std::string, std::string> m_fields;
    uint32_t m_majorVersion;
    uint32_t m_minorVersion;


};

}

#endif // REQUEST_H
