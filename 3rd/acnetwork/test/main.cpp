#include <acnetwork/networkclient.h>

#include <iostream>

int main(int argc, char *argv[])
{
    ac::NetworkClient client;
    client.onReady([](const ac::Response &response){
        std::cout << "-----------------------" << std::endl;
        std::cout << response.body() << std::endl;
    });
    client.onData([](const char *data, size_t size){
        std::cout << std::string(data, size) << std::endl;
    });
    client.sendRequest(ac::Request(ac::Url("http://bitbucket.org")));
    client.waitForFinish();

    return 0;
}
