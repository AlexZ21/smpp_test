#include "smpptestconnector.h"

#include <args/args.h>

#include <iostream>
#include <chrono>

int main(int argc, char **argv)
{
    // Парсинг аргументов коммандной строки
    args::ArgumentParser parser("This is a test smpp client program.", std::string());
    args::HelpFlag helpArg(parser, "help", "Display this help menu", {'h', "help"});
    args::ValueFlag<std::string> hostArg(parser, "host", "SMPP server host", {"host"});
    args::ValueFlag<uint16_t> portArg(parser, "port", "SMPP server port", {"port"});
    args::ValueFlag<std::string> systemIdArg(parser, "system-id", "SMPP server system id", {"system-id"});
    args::ValueFlag<std::string> passwordArg(parser, "password", "SMPP server system password", {"password"});

    try {
        parser.ParseCLI(argc, argv);
    } catch (args::Completion e) {
        std::cout << e.what() << std::endl;
        return 0;
    } catch (args::Help) {
        std::cout << parser << std::endl;
        return 0;
    } catch (args::ParseError e) {
        std::cerr << e.what() << std::endl;
        std::cerr << parser;
        return 1;
    }

    std::string host = hostArg.Get();
    if (host.empty())
        host = "127.0.0.1";

    uint16_t port = portArg.Get();
    if (port == 0)
        port = 2775;

    std::string systemId = systemIdArg.Get();
    if (systemId.empty())
        systemId = "test";

    std::string password = passwordArg.Get();
    if (password.empty())
        password = "test";

    SMPPTestConnector connector;

    if (!connector.connect(host, port, systemId, password)) {
        std::cerr << "Can't connect to server" << std::endl;
        return 1;
    }

    connector.sendPDU();

    return 0;
}
