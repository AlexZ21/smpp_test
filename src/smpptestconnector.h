#ifndef SMPPTESTCONNECTOR_H
#define SMPPTESTCONNECTOR_H

#include <acnetwork/tcpsocket.h>
#include <acnetwork/ipaddress.h>

//! acnetwork
//! https://bitbucket.org/AlexZ21/acnetwork

#include <thread>

struct PDU_BIND_RECEIVER
{
    int32_t commandLength = 0;
    int32_t commandId = 0;
    int32_t commandStatus = 0;
    int32_t sequenceNumber = 100;
    std::vector<char> systemId;
    std::vector<char> password;
    std::vector<char> systemType;
    int8_t interfaceVersion = 0;
    int8_t addrTon = 0;
    int8_t addrNpi = 0;
    std::vector<char> addressRange;
};

struct PDU_BIND_RECEIVER_RESP
{
    int32_t commandLength = 0;
    int32_t commandId = 0;
    int32_t commandStatus = 0;
    int32_t sequenceNumber = 0;
    std::string systemId;
};

class SMPPTestConnector
{
public:
    SMPPTestConnector();
    ~SMPPTestConnector();

    bool connect(const std::string &host, uint16_t port,
                 const std::string &systemId, const std::string &password);
    void sendPDU();

private:
    void sendingThreadFunc();
    void receivingThreadFunc();

private:
    ac::TcpSocket m_socket;
    ac::IpAddress m_addr;
    uint16_t m_port;
    bool m_connected;

    std::string m_systemId;
    std::string m_password;
    std::string m_systemType;

    std::thread m_sendingThread;
    std::thread m_receivingThread;

};

#endif // SMPPTESTCONNECTOR_H
