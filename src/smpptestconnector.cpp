#include "smpptestconnector.h"

#include <iostream>
#include <cstring>
#include <sstream>

// Прочитать int из массива данных
void readInteger(const std::vector<char> &data, size_t offset, int32_t &integer)
{
    for (int i = 0; i < 4; ++i)
        std::memcpy(&((char *)&integer)[3 - i], &data.data()[offset + i], 1);
}

// Записать int в массив данных
void writeInteger(std::vector<char> &data, size_t offset, int32_t integer)
{
    for (int i = 0; i < 4; ++i)
        std::memcpy(&data.data()[offset + i], &((char *)&integer)[3 - i], 1);
}

// Преобразовать строку в вектор и добавить \0
std::vector<char> stringToVector(const std::string &str)
{
    std::vector<char> vector;
    vector.resize(str.size() + 1);
    std::memcpy(vector.data(), str.data(), str.size());
    vector[vector.size() - 1] = '\0';
    return vector;
}

SMPPTestConnector::SMPPTestConnector() :
    m_port(0),
    m_connected(false),
    m_systemType("WWW")
{

}

SMPPTestConnector::~SMPPTestConnector()
{
    if (m_sendingThread.joinable())
        m_sendingThread.join();
    if (m_receivingThread.joinable())
        m_receivingThread.join();
}

bool SMPPTestConnector::connect(const std::string &host, uint16_t port,
                                const std::string &systemId, const std::string &password)
{
    m_addr = ac::IpAddress(host);
    m_port = port;
    m_connected = m_socket.connect(m_addr, m_port, std::chrono::seconds(1)) == ac::Socket::Done;
    m_systemId = systemId;
    m_password = password;
    return m_connected;
}

void SMPPTestConnector::sendPDU()
{
    if (!m_connected)
        return;

    m_sendingThread = std::thread(&SMPPTestConnector::sendingThreadFunc, this);
    m_receivingThread = std::thread(&SMPPTestConnector::receivingThreadFunc, this);
}

void SMPPTestConnector::sendingThreadFunc()
{
    // Формирование PDU
    PDU_BIND_RECEIVER pdu;
    pdu.commandId = 0x00000001;
    pdu.systemId = stringToVector(m_systemId);
    pdu.password = stringToVector(m_password);
    pdu.systemType = stringToVector(m_systemType);
    pdu.interfaceVersion = 34;
    pdu.addressRange.push_back('\0');

    pdu.commandLength = 19 + pdu.systemId.size() + pdu.password.size() +
            pdu.systemType.size() + pdu.addressRange.size();

    // Формирование массива данных из PDU
    std::vector<char> pduData;
    pduData.resize(pdu.commandLength);

    size_t offset = 0;
    // Header
    writeInteger(pduData, offset, pdu.commandLength);
    offset += 4;

    writeInteger(pduData, offset, pdu.commandId);
    offset += 4;

    writeInteger(pduData, offset, pdu.commandStatus);
    offset += 4;

    writeInteger(pduData, offset, pdu.sequenceNumber);
    offset += 4;

    // Body
    std::memcpy(&pduData.data()[offset], pdu.systemId.data(), pdu.systemId.size());
    offset += pdu.systemId.size();

    std::memcpy(&pduData.data()[offset], pdu.password.data(), pdu.password.size());
    offset += pdu.password.size();

    std::memcpy(&pduData.data()[offset], pdu.systemType.data(), pdu.systemType.size());
    offset += pdu.systemType.size();

    std::memcpy(&pduData.data()[offset], &pdu.interfaceVersion, 1);
    ++offset;

    std::memcpy(&pduData.data()[offset], &pdu.addrTon, 1);
    ++offset;

    std::memcpy(&pduData.data()[offset], pdu.addressRange.data(), pdu.addressRange.size());
    offset += pdu.addressRange.size();

    // Отправка данных
    size_t sent = 0;
    if (m_socket.send(pduData.data(), pduData.size(), sent) != ac::Socket::Done)
        std::cerr << "Error sending";
    std::cout << "Send bytes: " << sent << std::endl;
}

void SMPPTestConnector::receivingThreadFunc()
{
    // Получение данных
    const size_t bufferSize = 128;
    std::vector<char> buffer;
    buffer.resize(bufferSize);
    size_t received = 0;
    if (m_socket.receive(buffer.data(), bufferSize, received) != ac::Socket::Done) {
        std::cerr << "Error receiving";
        return;
    }

    std::cout << "Receive bytes: " << received << std::endl;

    // Формирование pdu из массива данных
    PDU_BIND_RECEIVER_RESP pdu;
    int32_t offset = 0;
    readInteger(buffer, offset, pdu.commandLength);
    offset += 4;

    readInteger(buffer, offset, pdu.commandId);
    offset += 4;

    readInteger(buffer, offset, pdu.commandStatus);
    offset += 4;

    readInteger(buffer, offset, pdu.sequenceNumber);
    offset += 4;

    // Если ответ содержит ошибку, то игнорируется чтение systemId
    int32_t size = (received - offset) - 1;
    if (size > 0) {
        pdu.systemId.resize((received - offset) - 1);
        std::memcpy(&pdu.systemId[0], &buffer.data()[offset], pdu.systemId.size());
    }

    if (pdu.commandStatus != 0)
        std::cerr << std::string("Receive error with code: " + std::to_string(pdu.commandStatus)) << std::endl;
    else
        std::cout << "Success connection" << std::endl;
}
